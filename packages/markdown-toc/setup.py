from setuptools import setup

setup(
    name='lektor-markdown-toc',
    version='0.1',
    author=u'Stavros Korokithakis',
    author_email='hi@stavros.io',
    license='MIT',
    py_modules=['lektor_markdown_toc'],
    entry_points={
        'lektor.plugins': [
            'markdown-toc = lektor_markdown_toc:MarkdownTocPlugin',
        ]
    }
)
